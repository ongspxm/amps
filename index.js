"use strict";
const fs = require('fs');
const http = require('http');
const express = require('express');
const WebSocket = require('ws');
const { exec } = require('child_process');

const app = express();
const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

// === global vars 
let songStart, title, songidx;
let songlist = [];

const audio_path = 'audio/';

// === global miscs
function getTime() {
    return (new Date()).getTime();
}

function getUpdate(action) {
    const tstamp = getTime();
    return JSON.stringify({
        tstamp, title, action,
        time: songStart,
    });
}

function sendUpdates() { 
    const data = getUpdate();
    wss.clients.forEach(ws => ws.send(data));
}

function loadDir() {
    songlist = fs.readdirSync('audio');
}

function duration(fname) {
    // returns a promise
    return new Promise((res, rej) => {
        console.log(fname);
        exec(`ffprobe "${fname}"`, (err, stdout, stderr) => {
            const time = stderr.split('Duration: ')[1]
                .split(',')[0].split(':');
            const ends = time[2].split('.');

            res(parseInt(time[0]) * 3600 
                + parseInt(time[1]) * 60 
                + parseFloat(time[2]));
        });
    });
}

// === main logic
loadDir();
if (songlist.length) {
    songidx = 0;
    title = songlist[0];
    resetTime();
}

const funcs = {
    // === song specific
    next() {
        songidx = (songidx + 1) % songlist.length;
        title = songlist[songidx];
        resetTime();
    },

    // qry.val in ms
    skip(qry) {
        resetTime(parseInt(qry.val) || 5);
    },

    // === player specific
    reload() {
        loadDir(); 
    },    

    // qry.val is name of the song
    select(qry) {
        title = qry.val || title;
        resetTime();
    },
};

// === next song jumping (playlist)
let next;
function resetTime(val) {
    if (val) {
        songStart += -1 * val;
    } else {
        songStart = getTime();
    }

    duration(audio_path + title).then(time => {
        const ms = time*1000 + songStart - getTime();

        clearTimeout(next);
        next = setTimeout(() => {
            funcs.next(); 
            sendUpdates();
        }, ms);
    });
}

// === endpoints
app.use('/audio', express.static(audio_path));
app.use('/app', express.static('build'));

app.use('/update', (req, res) => {
    const qry =  req.query;

    if (qry.action) {
        const func = funcs[qry.action];
        if (func) { func(qry); }
    }
    
    sendUpdates();
    res.send(getUpdate(qry.action));
});

app.use('/list', (req, res) => res.send(JSON.stringify({
    list: songlist
})));

wss.on('connection', (ws) => {
    ws.on('message', (msg) => {
        if (msg === 'time') {
            ws.send(`time ${getTime()}`);
        } else {
            ws.send(getUpdate());
        }
    });
});

server.listen(process.env.PORT || 3000);
