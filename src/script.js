const mobile = ('ontouchstart' in document.documentElement) ? 1 : 0;

function $(id) {
    return document.getElementById(id);
}

function getTime() {
    return (new Date()).getTime();
}

function start() {
    if ($('song').paused) {
        time = 0; ws.send('time');
    }
}

if (!"WebSocket" in window) {
    alert('websocket not supported');
}

let ws = new WebSocket(`ws://${window.location.host}`);

let tstamps = [];
function handleDelay(time) {
    if (tstamps.length<10) {
        tstamps.push(time); ws.send('time');
    } else {
        const leng = tstamps.length;
        const delay = ((tstamps[leng-1]-tstamps[leng-2])
            + (tstamps[leng-2] - tstamps[leng-3]))/2;
        
        sdelay = parseInt(getTime() - (time+(delay/2)));
        $('details').innerText = delay + ' - ' + sdelay;
        ws.send('get');
    }
}

function delay(val) {
    sdelay += val; 
    play();
}

function play() {
    const song = $('song');
    song.currentTime = (getTime() - sdelay - stime)/1000;
    song.play();

    $('details').innerText = sdelay+' - '+song.currentTime;
}

let stime, stitle, sdelay;
ws.onmessage = function(evt) {
    if (evt.data.startsWith('time')) {
        handleDelay(parseInt(evt.data.split(' ')[1]));
    } else {
        const msg = JSON.parse(evt.data);
        const song = $('song');
        const title = $('title');

        if (stitle !== msg.title) { 
            song.src = `/audio/${msg.title}`;
            title.innerText = msg.title;

            stime = 0;
            stitle = msg.title;
        }

        if (stime !== msg.time) { 
            stime = msg.time;
            play();
        }
    }
};

ws.onclose = function() {
    $('song').pause();
    alert('connection lost');
};
